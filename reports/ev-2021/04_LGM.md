In 2021, Libre Graphics Meeting (LGM) was held online and physical from 27th to 30th May.

The LGM is a yearly event about Free Software related to graphics. Organized every year since 2006, the goal of the Libre Graphics Meeting is to attract developers, artists and professionals using and improving Free Software for creating graphical work. The LGM fosters discussions between developers and users. Unlike many other events dedicated to Free Software, the LGM always has a strong artistic direction, with designers and artists showing their work along with the developers' work.

KDE’s community members participated at the LGM. Livio Fania illustrated a portrait demo using Krita, Ramón Miranda talked about Krita's growth and Arkengheist demonstrated live Video editing with Kdenlive.
