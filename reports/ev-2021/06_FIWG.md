<table width="100%">
    <tr>
        <td style="vertical-align: top;">
            <table class="table">
                <thead>
                    <tr>
                        <th><h3>Income (€):</h3></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Patrons:</td>
                        <td class="text-right">64,700.00</td>
                    </tr>
                    <tr>
                        <td>Supporting members & donations:</td>
                        <td class="text-right">104,713.28</td>
                    </tr>
                    <tr>
                        <td>Akademy:</td>
                        <td class="text-right">29,840.55</td>
                    </tr>
                    <tr>
                        <td>Other Events:</td>
                        <td class="text-right">14,199.48</td>
                    </tr>
                    <tr>
                        <td>GSoC and Code in:</td>
                        <td class="text-right">5,296.08</td>
                    </tr>
                    <tr>
                        <td>Other</td>
                        <td class="text-right">20,180.28</td>
                    </tr>
                    <tr>
                        <td><b>Total Income:</b></td>
                        <td class="text-right"><b>238,929.67</b></td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td style="vertical-align: top;">
            <table class="table">
                <thead>
                    <tr>
                        <th><h3>Expenses (€):</h3></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Personnel:</td>
                    <td class="text-right" style="color: red">-150,289.93</td>
                </tr>
                <tr>
                    <td>Akademy:</td>
                    <td class="text-right" style="color: red">-3,609.10</td>
                </tr>
                <tr>
                    <td>Sprints</td>
                    <td class="text-right" style="color: red">0.00</td>
                </tr>
                <tr>
                    <td>Other events:</td>
                    <td class="text-right" style="color: red">-666.35</td>
                </tr>
                <tr>
                    <td>Infrastructure:</td>
                    <td class="text-right" style="color: red">-9,046.76</td>
                </tr>
                <tr>
                    <td>Office:</td>
                    <td class="text-right" style="color: red">-7,079.03</td>
                </tr>
                <tr>
                    <td>Taxes and Insurance:</td>
                    <td class="text-right" style="color: red">-21,038.50</td>
                </tr>
                <tr>
                    <td>Other:</td>
                    <td class="text-right" style="color: red">-26,667.08</td>
                </tr>
                <tr>
                    <td><b>Total:</b></td>
                    <td class="text-right" style="color: red"><b>-218,396.75</b></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>

<table> <tr> <td> <img src="images/FiWG/fiwg_report_2021_piechart.png"/></td> </tr> </table>

### Report

The year of 2021 was financially largely uneventful. It was again dominated by the global pandemic, impacting KDE’s activities on both the income and expenses side of our finances. Overall, the financial result for 2021 is broadly similar to the previous year. This is good in that our organization continues to be of very strong financial health, with many options to fund activities going forward. It is, however, bad in that we had planned to significantly outspend our income in 2021, but in part due to a presently risk-averse ecosystem did not do so.

The reusable infrastructure for online events that was put in place in 2020 could be leveraged in 2021, as hoped, making our event activities in 2021 relatively low-cost. The response from our sponsors to our event activities was in line with previous years. As all events were held online, there were again very few travel expenses related to them.

We spent more money on personnel costs, partly because of a change in attribution of secondary salary costs (for example health insurance and personal work equipment) to personnel expenses, as opposed to taxes and insurances. The planned contracting only happened partially, leading to less spending than planned in this area.

The category of "Other" income grew, partially because grant payments from the environmental ministry of Germany, supporting the FEEP and BE4FOSS projects being conducted under our KDE Eco initiative, are accounted there. This additional income largely maps to expenses on the personnel side.

Donation income from KDE e.V.'s patrons and supporters, and individual donations, overall remained strong and stable in 2021. In these annual numbers, income via our patrons appears slightly inflated due to delayed payments of 2020 membership fees. However, throughout the year, KDE e.V. gained three new patrons while also losing one patron. Of particular note among the individual donations is a large single donation by one of our new patrons, PINE64, of almost 20.000 EUR.

For 2022, travel is expected to resurface as a significant expense as events return to in-person or hybrid formats. Contracting activities are also expected to pick up, following earlier plans. The budget for 2022 reflects the sustained goal of spending more aggressively in order to actively use our financial resources to support our community and further our aims.

**Financial Support:** If you or your company are interested in financially supporting the KDE Community on an ongoing basis, please visit the [Supporting Members page](https://ev.kde.org/getinvolved/supporting-members/) on the KDE e.V. website.
