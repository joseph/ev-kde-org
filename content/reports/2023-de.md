---
title: "Mitgliederversammlung KDE e.V. 2023"
layout: page
---

Die Mitgliederversammlung findet am Samstag, den 9. September 2023 um 15:00 CEST in den Räumlichkeiten des Vereins, Prinzenstraße 85 F, 10969 Berlin statt.
Mitglieder können auch online über die passwortgeschützten Open-Source-Videokonferenzsoftware BigBlueButton teilnehmen.
Tagesordnung und Präsentationen werden über das Open-Source-Tool OpenSlides verwaltet und bereitgestellt. In OpenSlides wird auch die Anwesenheit der Mitglieder registriert, die Vertreter-Stimmen verwaltet sowie die anstehenden Wahlen durchgeführt. Eine sichere, geheime, und überprüfbare Wahl ist durch dieses System gewährleistet. Beide Systeme werden auf KDE-Servern betrieben. Alle Mitglieder haben im Vorfeld der Versammlung Zugriff auf die beiden Systeme erhalten. Die Liste der Teilnehmer wird zur Dokumentation aus OpenSlides exportiert.

Zu Beginn der Veranstaltung sind 80 Mitglieder anwesend.
11 nicht anwesende Mitglieder haben einen Vertreter benannt.

## Tagesordnung

1. Begrüßung
2. Wahl des Versammlungsleiters
3. Bericht des Vorstands
   1. Bericht über Aktivitäten
   2. Bericht des Schatzmeisters
   3. Bericht der Rechnungsprüfer
   4. Entlastung des Vorstandes
4. Bericht der Vertreter und Arbeitsgruppen des Vereins
   1. Bericht der Vertreter in der KDE Free Qt Foundation
   2. Bericht der KDE Free Qt Working Group
   3. Bericht der System Administration Working Group
   4. Bericht der Community Working Group
   5. Bericht der Financial Working Group
   6. Bericht der Advisory Board Working Group
   7. Bericht der Fundraising Working Group
5. Wahl des Vorstands
6. Wahl der Rechnungsprüfer
7. Wahl der Vertreter in der KDE Free Qt Foundation
8. Abstimmung zur Möglichkeit Mitgliederversammlungen nach §32 BGB online abhalten zu können
9. Abstimmungen über Änderungen der Bestimmungen von Online-Abstimmungen
   1. Abstimmung über Änderung der Wahlbenachrichtigung
   2. Abstimmung über Änderung der Voraussetzungen um an der Wahl teilzunehmen
10. Verschiedenes

## Protokoll

Alle Berichte wurden den Mitgliedern im Vorfeld per Email zugesandt und am 15.07.2023 zusätzlich auf der durch den Verein organisierten Konferenz [Akademy](https://akademy.kde.org/2023) öffentlich vorgestellt.

### Begrüßung und Wahl des Versammlungsleiters

Um 15:05 eröffnet der Vorsitzende des Vorstands, Aleix Pol i Gonzàlez, die Versammlung
und schlägt Harald Sitter als Versammlungsleiter vor.

Die Mitglieder stimmen über Harald als Versammlungsleiter ab:

* Stimmen dafür:    **87**
* Stimmen dagegen:  **0**
* Enthaltungen:     **2**
* Abstimmungsteilnehmer:  **89**

Harald Sitter ist zum Versammlungsleiter gewählt.

Der Versammlungsleiter stellt fest, dass die Einladung zur Mitgliederversammlung ordnungsgemäß und fristgerecht - per Emailversand am 28.07.2023 mit vorläufiger und erneut am 27.08.23 mit finaler Tagesordnung erfolgt ist.

Es gibt keine Einsprüche gegen die Tagesordnung und es wurden keine weiteren Punkte angebracht.

Der Versammlungsleiter benennt David Redondo als Protokollant und stellt fest, dass das satzungsgemäße Quorum erfüllt ist.

### Bericht des Vorstands

#### Bericht über Aktivitäten

Der aktuelle Vorstand besteht aus Aleix Pol i Gonzàlez (Vorsitzender), Eike Hein (Schatzmeister und Stellvertreter des Vorsitzenden), Lydia Pintscher (Stellvertreterin des Vorsitzenden), Adriaan de Groot und Nathaniel Graham.
Mit dieser Mitgliederversammlung enden die Amtszeiten von Aleix Pol i Gonzàlez, Eike Hein, und Lydia Pintscher.

6 neue aktive Mitglieder wurden in den Verein seit der letzten Mitgliederversammlung aufgenommen.

Der Verein hat im Gegensatz zu den Vorjahren wieder neue Fördermitglieder gewonnen. Insgesamt gibt es nun 53 Fördermitglieder - 5 mehr als im letzten Jahr.
Zusätzlich hat der Verein 67 Unterstützer auf Github und 130 wiederkehrende Spender auf der Platform Donorbox.
Es wurden drei neue Patrone gewonnen mbition, Kubuntu Focus und g10code und sind somit auch teil des Advisory Boards. Es wurden jedoch auch zwei verloren, enioka Haute Couture und pine64.
Enioka Haute Couture bleibt dem den Verein jedoch weiter als Unterstützer treu.

Der Verein unterhält vielfältige Partnerschaften mit anderen Organisationen und ist Mitglied in verschiedenen Organisationen im Bereich Open Source.

Der Verein hat zwei Angestellte und neun freie Mitarbeiter. Erstere beide sind Petra Gillert ist als Assistenz des Vorstands und Joseph P. De Veaugh-Geiss
, der nach dem Ende des Blaue-Engel Projekts nun für das KDE Eco Projekt zuständig und allgemein als Projekt und Community Manager tätig ist.
Weiterhin für den Verein tätig sind, Dina Nouskali als Event-Koordinatorin, Adam Szopa als Projekt-Koordinator und Ingo Klöcker als App Store Engineer, sowie
Aniqa Khokhar und Paul Brown als Marketing-Berater.
Neue Mitarbeiter sind Thiago Masato Costa Sueto für Dokumentation, Natalie Clarius für Hardware Integration und Nicolas Fella als Software Platform Engineer.
Lana Lutz hat infolge des erwähnten Ende des Blaue Engel Projekts den Verein verlassen.

Der Vorstand dankt den Organisatoren der diesjährigen Akademy Konferenz. Für nächstes Jahr wird noch ein Ausrichtungsort gesucht.
Im letzten Jahr gab es wieder einige Sprints aber nicht so viele wie erwartet. Der Vorstand ermutigt weiter Sprints zu organisieren.
Auch wurden wieder einige Konferenzen und Messen besucht.

Ein Fokus lag auf der Einsammlung von Spenden. Wie bereits erwähnt wurden drei neue Patrone gefunden und die Donorbox Platform genutzt.
Ende des Jahres wurde eine Spendenkampagne veranstaltet die erfolgreich war, ebenso wie eine Kampagne für Kdenlive. Beginnend mit dem dem Jahr 2025
sollen die Preise für Fördermitgliedschaften erhöht werden. Die Einwerbung von Fördermitteln war weniger erfolgreich, jedoch wird dies in
Zukunft weiter versucht.

Der Vorstand begrüßt, dass Sprints wieder stattfinden, das Blaue Engel Projekt erfolgreich beendet wurde und die Spendenkampagnen erfolgreich waren.
Es wurde erreicht alle geplanten Stellen zu füllen und der Vorstand sieht erhöhtes Interesse in der Software der Gemeinschaft von seiten
von Hardware Herstellern.
Auch wird der Fortschritt der Gemeinschaft in der Portierung der Software zu Qt6 hervorgehoben.

Als Schlüsselpunkte für die Zukunft sieht der Vorstand die Unterstützung der Ziele die sich die Gemeinschaft gegeben hat,
sichern der finanziellen Situation und der Maximierung der Möglichkeiten die Treffen wie Akademy, Linux App Summit und Sprints bieten.
Weitere Punkte sind die Stellen die im "Make a Living" Prozess gestartet wurden und den Abstand zu den Nutzern der Software zu verringern.

#### Bericht des Schatzmeisters

Eike Hein erläutert kurz den Finanzbericht, den er mit Hilfe der Financial Working Group und des Vorstands erstellt hat und verweist
auf Stellen, an denen weitere detaillierte Informationen gefunden werden können.

##### Finanzjahr 2022

Der Verein hat endlich das Ziel erreicht höhere Ausgaben als Einnahmen zu haben, absolut haben sich sowohl Einnahmen als auch Ausgaben erhöht. 
Akademy 2022 was nicht kostendeckend, jedoch hat Akademy 2023 dem Verein Geld eingebracht.
Die Einnahmen konnten besser voraus gesagt und geplant werden als vorher.

##### Finanzplan 2023

Die Spendensituation verbessert sich und die Preise für eine Fördermitgliedschaft soll erhöht werden.
Es sollen keine weiteren Stellen ausgeschrieben werden.
Die monetären Reserven sollen weiter reduziert werden und gleichzeitig die Einnahmen den Ausgaben angeglichen werden.
Sollte sich die Situation verschlechtern (wie während der Corona Pandemie) bestehen Möglichkeiten sich dieser schnell anzupassen.

#### Bericht der Kassenprüfer
Der Bericht der Kassenprüfer wurde vor der Versammlung den Mitglieder bereits am 13.06.2023 per Email zugesandt.

Die Kassenprüfer haben am 03.06.2023 die Prüfung bei einem Treffen im Büro des Vereins mit Lydia Pintscher und Petra Gillert durchgeführt und haben Einsicht in die Bücher genommen.
Es wird eine ordnungsgemäße Buchführung attestiert. Alle Fragen der Kassenprüfer wurden zu deren Zufriedenheit beantwortet würden sich jedoch einige Änderungen für die Zukunft wünschen
um ihre Arbeit zu erleichtern.

Die Kassenprüfer empfehlen die Entlastung des Vorstands für das Finanzjahr 2022.

#### Entlastung des Vorstands

Der Versammlungsleiter bittet um Meldung, ob Einwände gegen die Entlastung bestehen. Es gibt keine Meldungen.

Drei Mitglieder sind inzwischen der Versammlung beigetreten.

Die Abstimmung wird jetzt durchgeführt. Die Mitglieder des Vorstandes nehmen an dieser Abstimmung nicht teil. Das Abstimmungsergebnis lautet:

* Für die Entlastung:     **84**
* Gegen die Entlastung:   **0**
* Enthaltungen:           **0**
* Abstimmungsteilnehmer:  **0**

Mit diesem Ergebnis ist der Vorstand für die vergangene Berichtsperiode entlastet.

### Bericht der Vertreter und Arbeitsgruppen des Vereins

Hier werden von den einzelnen Gruppen lediglich Ergänzungen gegeben, die nicht für die Öffentlichkeit bestimmt sind und daher nicht in den vorab versandten Berichten und der öffentlichen Präsentation enthalten sind.

   1. Bericht der Vertreter in der KDE Free Qt Foundation
   2. Bericht der KDE Free Qt Working Group
   3. Bericht der System Administration Working Group
   4. Bericht der Community Working Group
   5. Bericht der Financial Working Group
   6. Bericht der Advisory Board Working Group
   7. Bericht der Fundraising Working Group

#### Bericht der Community Working Group
Es gab eine Beschwerde über einen Maintainer, der nicht auf Anfragen der Arbeitsgruppe reagiert aber auch nicht weiter in der Community aktiv ist.
Es gibt Interessenten die der Community Working Group beitreten wollen.

#### Andere Gruppen

Die anderen Vertreter und Arbeitsgruppen haben über die in den öffentlich zugänglichen Informationen keine weiteren Punkte zu berichten.

### Wahl des Vorstandes
Die Amtszeiten der Vorstände Aleix Pol i Gonzàlez, Eike Hein und Lydia Pintscher enden mit der Versammlung. Aleix, Eike und Lydia stellen sich für eine Wiederwahl zur Verfügung. Es gibt keine weiteren Kandidaten.

Die Kandidaten stellen sich kurz vor.

Es wird geheim abgestimmt. Die abgegebenen Stimmen ergeben folgendes Wahlergebnis für die Kandidaten:

* Aleix Pol i Gonzàlez:   **86**
* Eike Hein:              **86**
* Lydia Pintscher         **86**
* Wahlteilnehmer:         **86**

Aleix, Eike und Lydia nehmen nach Rückfrage des Versammlungsleiters die Wahl an die Wahl an.

### Wahl der Rechnungsprüfer

Die beide bisherigen Rechnungsprüfer Andreas Cord-Landwehr und Thomas Baumgart stellen sich für eine Wiederwahl. Es gibt keine weiteren Kandidaten.

Die abgegebenen Stimmen ergeben folgendes Wahlergebnis für die Kandidaten:

* Andreas Cord-Landwehr:   **87**
* Thomas Baumgart:         **86**
* Wahlteilnehmer:          **88**

Thomas nimmt nach Rückfrage des Versammlungsleiters die Wahl an. Andreas ist nicht anwesend hat jedoch vor der Versammlung mit seiner Kandidatur bekannt gegeben, dass
er im Falle einer Wahl diese auch annhmenen werde. Dies hat er auch nach der Versammlung getan.

### Wahl der Vertreter in der KDE Free Qt Foundation

Olaf Schmidt-Wischhöfer und Albert Astals Cid stehen für eine Wiederwahl zur Verfügung. Es gibt keine weiteren Kandidaten.

Die abgegebenen Stimmen ergeben folgendes Wahlergebnis für die Kandidaten:

* Olaf Schmidt-Wischhöfer: **89**
* Albert Astals Cid:       **88**
* Wahlteilnehmer:          **89**

Beide Kandidaten nehmen die Wahl nach Rückfrage durch den Versammlungsleiter an.

### Abstimmung zur Möglichkeit Mitgliederversammlungen nach §32 BGB online abhalten zu können
Der vom Vorstand eingebrachte Antrag wurde den Mitgliedern vor der Versammlung per Email zur Verfügung gestellt und auch auf der Mailingliste der Mitglieder im Vorfeld diskutiert.
Die Abstimmung über den Antrag ergibt folgendes Ergebnis:

* Annahme des Antrages:    **89**
* Ablehnung des Antrages:  **2**
* Enthaltungen:            **0**
* Abstimmungsteilnehmer:   **91**

Somit ist der Antrag von der Mitgliedschaft angenommen.

### Abstimmungen über Änderungen der Bestimmungen von Online-Abstimmungen
#### Abstimmung über Änderung der Wahlbenachrichtigung
Der vom Vorstand eingebrachte Antrag zur Änderung von §2.3 der Bestimmungen zu Online Abstimmungen wurde den Mitgliedern vor der Versammlung per Email zur Verfügung gestellt und auch auf der Mailingliste der Mitglieder im Vorfeld diskutiert.
Die Abstimmung über den Antrag ergibt folgendes Ergebnis:

* Annahme des Antrages:    **85**
* Ablehnung des Antrages:  **3**
* Enthaltungen:            **3**
* Abstimmungsteilnehmer:   **91**

Somit ist der Antrag von der Mitgliedschaft angenommen.

#### Abstimmung über Änderung der Voraussetzungen um an der Wahl teilzunehmen
Der vom Vorstand eingebrachte Antrag Änderung von §4 der Bestimmungen zu Online Abstimmungen wurde den Mitgliedern vor der Versammlung per Email zur Verfügung gestellt und auch auf der Mailingliste der Mitglieder im Vorfeld diskutiert.
Die Abstimmung über den Antrag ergibt folgendes Ergebnis:

* Annahme des Antrages:    **84**
* Ablehnung des Antrages:  **1**
* Enthaltungen:            **3**
* Abstimmungsteilnehmer:   **88**

Somit ist der Antrag von der Mitgliedschaft angenommen.

### Verschiedenes
 
Es werden keine weiteren Punkte diskutiert.

### Schluss der Versammlung
Harald Sitter beendet die Versammlung um 17:14.
<br />
<br />
<br />
<br />
<br />
<br />
David Redondo &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Harald Sitter<br/>
(Protokollant)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
(Versammlungsleiter)<br/>
