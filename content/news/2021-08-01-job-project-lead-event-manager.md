---
title: 'KDE e.V. is looking for a project lead/event manager for environmental sustainability project'
date: 2021-08-01 13:00:00 
noquote: true
categories: [Hiring]
---

> Edit 2021-10-01: applications for this position **are closed**.

KDE e.V., the non-profit organisation supporting the KDE community, is looking for a great person to run a project related to the environmental sustainability of our software.
The position we are looking to fill immediately is that of a project lead who can also do some event management duties.
Please see the [job ad for the project lead]({{ '/resources/jobad-projectlead2021-2.pdf' | prepend: site.url }}) for more details about this employment opportunity. 
We are looking forward to your application.
